<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;
use CommunecterController;

/**
 * StatController.php
 *
 * @author: Childéric THOREAU <childericthoreau@gmail.com>
 * Date: 3/18/16
 * Time: 12:25 AM
 */
class StatController extends CommunecterController {

	public function actions()
	{
	    return array(
	        'createglobalstat'    	 => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\stat\CreateGlobalStatAction::class,
	        'getstatjson'    	 	 => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\stat\GetStatJsonAction::class,
	        'chartglobal'    	 	 => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\stat\ChartGlobalAction::class,
	        'chartlogs'    	 	 	 => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\stat\ChartLogsAction::class,
	    );
	}
}