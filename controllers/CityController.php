<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * SiteController.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:25 AM
 */
class CityController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'index'       			=> 'citizenToolKit.controllers.city.IndexAction',
	        'detail'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\DetailAction::class,
	        'directory'    		 	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\DirectoryAction::class,
	        'calendar'      		=> 'citizenToolKit.controllers.city.CalendarAction',
	        'statisticpopulation' 	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\StatisticPopulationAction::class,
	        'getcitydata'     		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetCityDataAction::class,
	        'getcityjsondata'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetCityJsonDataAction::class,
	        'getcitiesdata'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetCitiesDataAction::class,
	        'statisticcity'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\statisticCityAction::class,
	        'opendata'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\OpenDataAction::class,
	        'getoptiondata'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetOptionDataAction::class,
	        'getlistoption'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetListOptionAction::class,
	        'getpodopendata'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetPodOpenDataAction::class,
	        'addpodopendata'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\AddPodOpenDataAction::class,
	        'getlistcities'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetListCitiesAction::class,
	        'creategraph'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\CreateGraphAction::class,
	        'graphcity'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GraphCityAction::class,
	        'updatecitiesgeoformat' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\UpdateCitiesGeoFormatAction::class,
	        'getinfoadressbyinsee'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetInfoAdressByInseeAction::class,
	        'cityexists'  			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\CityExistsAction::class,
	        'autocompletemultiscope'=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\AutocompleteMultiScopeAction::class,
	        'save'       			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\SaveAction::class,
	        'getdepandregion'=> 'citizenToolKit.controllers.city.GetDepAndRegionAction',
	    );
	}
}