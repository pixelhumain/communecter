<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

	use CommunecterController;

    /**
 * PodController.php
 *
 */

class PodController extends CommunecterController {

	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'slideragenda'      => 'citizenToolKit.controllers.pod.SliderAgendaAction',
	        'photovideo'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\pod\PhotoVideoAction::class,
	        'fileupload'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\pod\FileUploadAction::class,
	        'activitylist'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\pod\ActivityListAction::class,
	    );
	}

}
?>