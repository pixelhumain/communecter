<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use PixelHumain\PixelHumain\modules\citizenToolKit\models\Admin;
use CommunecterController;
use Cron;
use Mail;
use MongoId;
use Organization;
use PHDB;
use PHType;
use Rest;
use Yii;

class PersonController extends CommunecterController {

  public $hasSocial = false;
  public $loginRegister = true;

  
  public function accessRules() {
    return array(
      // not logged in users should be able to login and view captcha images as well as errors
      array('allow', 'actions' => array('index','graph','register','register2')),
      // logged in users can do whatever they want to
      array('allow', 'users' => array('@')),
      // not logged in users can't do anything except above
      array('deny'),
    );
  }

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'index'              => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\IndexAction::class,
	        'login'     	       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\LoginAction::class,
          'logged'              => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\LoggedAction::class,
          'sendemail'          => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\SendEmailAction::class,
	        'logout'     	       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\LogoutAction::class,
	        'authenticate'       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\AuthenticateAction::class,
          'detail'             => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\DetailAction::class,
	        'follows'  		       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\FollowsAction::class,
	        'disconnect'         => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\DisconnectAction::class,
	        'activate'  	       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\ActivateAction::class,
	        'register'  	       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\RegisterAction::class,
	        'getnotification'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\GetNotificationAction::class,
          'getthumbpath'       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\GetThumbPathAction::class,
          'invite'  		       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\InviteAction::class,
	        'invitation'         => 'citizenToolKit.controllers.person.InvitationAction',
	        'updatefield'	       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\UpdateFieldAction::class,
          'update'             => 'citizenToolKit.controllers.person.UpdateAction',
          'directory'          => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\DirectoryAction::class,
          'data'               => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\DataAction::class,
          'chooseinvitecontact'=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\ChooseInviteContactAction::class,
          'changepassword'     => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\ChangePasswordAction::class,
          'changerole'         => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\ChangeRoleAction::class,
          'checkusername'      => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\CheckUsernameAction::class,
          'checklinkmailwithuser'=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\CheckLinkMailWithUserAction::class,
          'validateinvitation' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\ValidateInvitationAction::class,
          'getuseridbymail'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\GetUserIdByMailAction::class,
          "updatesettings"     => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\UpdateSettingsAction::class,
          "updateprofil"       => 'citizenToolKit.controllers.person.UpdateProfilAction',
          "updatewithjson"     => 'citizenToolKit.controllers.person.UpdateWithJsonAction',
          "telegram"           => 'citizenToolKit.controllers.person.TelegramAction',
          "updatemultitag"     => 'citizenToolKit.controllers.person.UpdateMultiTagAction',
          "updatemultiscope"   => 'citizenToolKit.controllers.person.UpdateMultiScopeAction',
          "sendinvitationagain"=> 'citizenToolKit.controllers.person.SendInvitationAgainAction',
          'get'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\GetAction::class,

	    );
	}

public function actionAbout(){

  	$person = PHDB::findOne(PHType::TYPE_CITOYEN, array( "_id" => new MongoId(Yii::app()->session["userId"]) ) );
  	$tags = PHDB::findOne( PHType::TYPE_LISTS,array("name"=>"tags"), array('list'));
  	
  	return $this->render( "about" , array("person"=>$person,'tags'=>json_encode($tags['list'])) );

}
public function actionInitDataPeople()
{
    //inject Data brute d'une liste de Person avec Id
    $import = Admin::initModuleData( $this->module->id, "personNetworking", PHType::TYPE_CITOYEN,true );
    $import = Admin::initModuleData($this->module->id, "organizationNetworking", Organization::COLLECTION);

    $result = ( $import["errors"] > 0 ) ? false : true;
    return Rest::json( $import );
}

  public function actionInitDataPeopleAll()
  {
    //inject Data brute d'une liste de Person avec Id
    $import = Admin::initMultipleModuleData( $this->module->id, "personNetworkingAll", true );

    $result = ( $import["errors"] > 0 ) ? false : true;
    return Rest::json( $import );
  }

  public function actionImportMyData()
  {
    $base = 'upload'.DIRECTORY_SEPARATOR.'export'.DIRECTORY_SEPARATOR.Yii::app()->session["userId"].DIRECTORY_SEPARATOR;
    if( Yii::app()->session["userId"] && file_exists ( $base.Yii::app()->session["userId"].".json" ) )
    {
      //inject Data brute d'une liste de Person avec Id
      $res = array("result"=>true, "msg"=>"import success");//Admin::initMultipleModuleData( $this->module->id, "personNetworkingAll", true );
      //$res["result"] = ( isset($res["errors"]) && $res["errors"] > 0 ) ? false : true;
    } else 
      $res = array("result"=>false, "msg"=>"no Data to Import");

    return Rest::json( $res );
  }

  public function actionClearInitDataPeopleAll()
  {
    //inject Data brute d'une liste de Person avec Id
    $import = Admin::initMultipleModuleData( $this->module->id, "personNetworkingAll", true,true,true );

    return Rest::json( $import );
  }
 
 public function actionSendMail()
  {
    foreach ($_POST['mails'] as $value) 
    {
      if (filter_var($value, FILTER_VALIDATE_EMAIL)) 
      {

        $params = array(
        "type" => Cron::TYPE_MAIL,
        "tpl"=>'sharePH',
          "subject" => 'Rejoint-nous sur PixelHumain',
          "from"=>Yii::app()->params['adminEmail'],
          "to" => $value,
          "tplParams" => array( "message"=>$_POST['textmail'],
                                "personne"=>Yii::app()->session['userId'])
          );
      
        Mail::schedule($params);



        /*$message = new YiiMailMessage;
        $message->setSubject("Communecte toi");
        $message->setBody($_POST['textmail']."<br/><a href='http://pixelhumain.com'>PixelHumain</a>", 'text/html');
        $message->addTo($value);
        $message->from = Yii::app()->params['adminEmail'];

        Yii::app()->mail->send($message);
       
        Yii::app()->session["mailsend"] = true;*/

      }
    
    }
    return Rest::json(array('result'=> true));
  }

}