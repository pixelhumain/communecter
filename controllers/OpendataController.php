<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * OpenDataController.php
 *
 * @author: sylvain.barbot@gmail.com
 * Date: 10/05/15
 * Time: 12:25 AM
 */
class OpendataController extends CommunecterController {
  
	public function actions() {
	    return array(
			'getcitiesbypostalcode'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\opendata\GetCitiesByPostalCodeAction::class,
			'getcitiesgeoposbypostalcode'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\opendata\GetCitiesGeoPosByPostalCodeAction::class,
			'getcountries'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\opendata\GetCountriesAction::class,
	    );
	}

}