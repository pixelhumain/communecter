<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;
use Yii;

/**
 * SiteController.php
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 7/23/12
 * Time: 12:25 AM
 */
class DocumentController extends CommunecterController {
  
	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
  	}

	public function actions()
	{
	    return array(
	        //CTK actions
	        'list'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\document\ListAction::class,
	        'save'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\document\SaveAction::class,
	        'deletedocumentbytid'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\document\DeleteDocumentByIdAction::class,
            'delete'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\document\DeleteAction::class,
            'upload'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\document\UploadAction::class,
            'getlistbyid'           => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\document\GetListByIdAction::class,

	        'resized' => array (
	            'class'   => 'ext.resizer.ResizerAction',
	            'options' => array(
	                // Tmp dir to store cached resized images 
	                'cache_dir'   => Yii::getPathOfAlias('webroot') . '/assets/',	 
	                // Web root dir to search images from
	                'base_dir'    => Yii::getPathOfAlias('webroot') . '/',
	            )
	        )
	    );
	}

    //Not use ???
    function clean($string) {
       $string = preg_replace('/  */', '-', $string);
       $string = strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'); // Replaces all spaces with hyphens.
       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
    

}