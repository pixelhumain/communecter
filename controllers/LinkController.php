<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * LinkController.php
 *
 * Manage Links between Organization, Person, Projet and Event
 *
 * @author: Sylvain Barbot <sylvain@pixelhumain.com>
 * Date: 05/05/2015
 */
class LinkController extends CommunecterController {

	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
	}
	public function actions()
	{
	    return array(
	        'removemember'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\RemoveMemberAction::class,
	        "removerole"		=> 'citizenToolKit.controllers.link.RemoveRoleAction',
			'removecontributor' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\RemoveContributorAction::class,
			'disconnect' 		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\DisconnectAction::class,
			//New Actions
			'connect' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\ConnectAction::class,
			'multiconnect' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\MultiConnectAction::class,
			'follow' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\FollowAction::class,
			'validate' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\ValidateAction::class,
			'favorite' 			=> 'citizenToolKit.controllers.link.FavoriteAction',
	    );
	}
}