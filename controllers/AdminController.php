<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * AdminController.php
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 7/23/12
 * Time: 12:25 AM
 */
class AdminController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'index'       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\IndexAction::class,
	        'directory'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\DirectoryAction::class,
	        'mailerrordashboard'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\MailErrorDashboardAction::class,
	        'switchto'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\SwitchtoAction::class,
	        'delete'      => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\DeleteAction::class,
	        'activateuser'=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\ActivateUserAction::class,
	        'importdata'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\ImportDataAction::class,
	        'previewdata'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\PreviewDataAction::class,
	        'importinmongo'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\ImportInMongoAction::class,
	        'assigndata'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\AssignDataAction::class,
	        'checkdataimport'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\CheckDataImportAction::class,
	        'openagenda'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\OpenAgendaAction::class,
	        'checkventsopenagendaindb'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\CheckEventsOpenAgendaInDBAction::class,
	        'importeventsopenagendaindb'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\ImportEventsOpenAgendaInDBAction::class,
	        'checkgeocodage'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\CheckGeoCodageAction::class,
	        'getentitybadlygeolocalited'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\GetEntityBadlyGeoLocalitedAction::class,
	        'getdatabyurl' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\GetDataByUrlAction::class,
	        'adddata' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\AddDataAction::class,
	        'adddataindb' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\AddDataInDbAction::class,
	        'createfileforimport' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\CreateFileForImportAction::class,
	        'sourceadmin' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\SourceAdminAction::class,
	        'moderate' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\ModerateAction::class,
	        'checkcities' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\CheckCitiesAction::class,
	        'checkcedex' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\CheckCedexAction::class,
	        'downloadfile' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\DownloadFileAction::class,
			'statistics' => 'citizenToolKit.controllers.admin.StatisticsAction',
			'createfile' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\CreateFileAction::class,
			'cities' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin\CitiesAction::class,

	    );
	}
}