<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * DefaultController.php
 *
 * OneScreenApp for Communecting people
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class SigController extends CommunecterController {

    public function beforeAction($action) {
		  return parent::beforeAction($action);
  	}

  	public function actions()
  	{
      return array(
          'network'          	 => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\NetworkAction::class,
          'companies'          => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\CompaniesAction::class,
          'state'      		     => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\StateAction::class,
          'events'      	     => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\EventsAction::class,
          'getmyposition'      => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\GetMyPositionAction::class,
          'getlatlngbyinsee'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\GetLatLngByInseeAction::class,
          'getinseebylatlng'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\GetInseeByLatLngAction::class,
          'getcodeinseebycityname' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\GetCodeInseeByCityNameAction::class,
          'getcountrybylatlng' => 'citizenToolKit.controllers.sig.GetCountryByLatLngAction',
          'showmynetwork'      => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\ShowMyNetworkAction::class,
          'ShowNetworkMapping' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\ShowNetworkMappingAction::class,
          'ShowLocalCompanies' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\ShowLocalCompaniesAction::class,
          'ShowLocalState'     => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\ShowLocalStateAction::class,
          'ShowLocalEvents'		 => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\ShowLocalEventsAction::class,
          'initDatanetworkmapping' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\InitDataNetworkMappingAction::class,
          'updateentitygeoposition' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig\UpdateEntityGeopositionAction::class,

      );
  	}
	
}
